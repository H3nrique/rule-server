package br.com.fabricads.poc.config.infinispan;

import br.com.fabricads.poc.model.ItemPostal;
import org.infinispan.protostream.MessageMarshaller;

import java.io.IOException;

/**
 * Created by h3nrique on 01/02/17.
 */
public class ItemPostalMarshaller implements MessageMarshaller<ItemPostal> {

    @Override
    public Class<? extends ItemPostal> getJavaClass() {
        return ItemPostal.class;
    }

    @Override
    public String getTypeName() {
        return ItemPostal.class.getName();
    }

    @Override
    public ItemPostal readFrom(ProtoStreamReader protoStreamReader) throws IOException {
        return null;
    }

    @Override
    public void writeTo(ProtoStreamWriter protoStreamWriter, ItemPostal itemPostal) throws IOException {

    }
}
