package br.com.fabricads.poc.config.drools;

/**
 * A subset of 'path-based' resource types that can be defined by a String for
 * adding to a Drools resource builder.
 *
 * @author Stephen Masters
 */
public enum ResourcePathType {


    CLASSPATH,
    FILE,
    URL;

}