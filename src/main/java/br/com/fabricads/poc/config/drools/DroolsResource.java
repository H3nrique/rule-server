package br.com.fabricads.poc.config.drools;

/**
 * A simple representation of a Drools resource.
 *
 * @author Stephen Masters
 */
public class DroolsResource {

    private String path;
    private ResourcePathType pathType;
    private String username = null;
    private String password = null;

    /**
     *
     * @param path
     *            The path to this resource.
     * @param pathType
     *            The type of path (FILE, URL, etc).
     */
    public DroolsResource(String path, ResourcePathType pathType) {
        this.path = path;
        this.pathType = pathType;
    }

    /**
     * Constructor for when the resource is secured. i.e. When the resource is a
     * Guvnor package being accessed via the REST API, and Guvnor requires a
     * user name and password to connect.
     *
     * @param path
     *            The path to this resource.
     * @param pathType
     *            The type of path (FILE, URL, etc).
     * @param type
     *            The type of resource (DRL, Binary package, DSL, etc)
     * @param username
     *            The user name for connecting to the resource.
     * @param password
     *            The password for connecting to the resource.
     */
    public DroolsResource(String path, ResourcePathType pathType,
                          String username, String password) {
        this.path = path;
        this.pathType = pathType;
        this.username = username;
        this.password = password;
    }

    /**
     * @return The path to this resource.
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     *            The path to this resource.
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return The type of path (FILE, URL, etc).
     */
    public ResourcePathType getPathType() {
        return pathType;
    }

    /**
     * @param pathType
     *            The type of path (FILE, URL, etc).
     */
    public void setPathType(ResourcePathType pathType) {
        this.pathType = pathType;
    }

    /**
     * @return The user name for connecting to the resource.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username
     *            The user name for connecting to the resource.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return The password for connecting to the resource.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password
     *            The password for connecting to the resource.
     */
    public void setPassword(String password) {
        this.password = password;
    }

}