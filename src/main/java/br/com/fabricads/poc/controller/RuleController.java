package br.com.fabricads.poc.controller;


import br.com.fabricads.poc.services.RuleService;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
@Slf4j
@RestController
@RequestMapping(value = "/process")
public class RuleController {

    @Autowired
    private RuleService ruleService;

    private ExecutorService exec = Executors.newCachedThreadPool(new ThreadFactoryBuilder().setNameFormat("RuleController-%d").build());

    /**
     * echo $(curl -s -X POST -F 'json={}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"json":"valor","idade":"17"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"json":"valor","idade":"18"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"60510101"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"03694090"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"04654010"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"AtualizaInteligencia","codigoPostal": {"packageName": "br.com.fabricads.poc.rules", "typeName": "CodigoPostal", "id": 3, "cep1": "04654", "cep2": "010", "logradouro": "Rua Camaratinga", "cidade": "São Paulo", "uf": "SP" }}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ItemPostal","cep":"04654010"}' http://localhost:9080/rule/process/generic)
     *
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"200","descricao":"CAMISA FARDA UNIFORME","cnae":"100"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"100","descricao":"CAMISA FARDAMENTO UNIFORME","cnae":"100"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"200","descricao":"CAMISA UNIFORME","cnae":"100"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"200","descricao":"TESTE","cnae":"100"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"ObjetoEntrada","ncm":"300","descricao":"TESTE","cnae":"100"}' http://localhost:9080/rule/process/generic)
     *
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"Transaction","id":"1","value":"10","operation":"DEBIT","entryPoint":"BankStream"}' http://localhost:9080/rule/process/generic)
     * echo $(curl -s -X POST -F 'json={"packageName":"br.com.fabricads.poc.model","typeName":"Transaction","id":"2","value":"10","operation":"DEBIT","entryPoint":"BankStream"}' http://localhost:9080/rule/process/generic)
     *
     * @param json
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, value = "/generic")
    public DeferredResult<ResponseEntity<String>> executeGenericRules(@RequestParam(required = true) String json) {

        final DeferredResult<ResponseEntity<String>> result = new DeferredResult<>();
        Runnable worker = () -> {
            try {
                String resultJson = ruleService.fireFules(json);
                result.setResult(new ResponseEntity<String>(resultJson, HttpStatus.ACCEPTED));
            } catch (RuntimeException err) {
                result.setResult(new ResponseEntity<String>("Internal Error.", HttpStatus.INTERNAL_SERVER_ERROR));
            } catch (Exception err) {
                result.setResult(new ResponseEntity<String>(err.getMessage(), HttpStatus.BAD_REQUEST));
            }
        };

        exec.submit(worker);
        return result;
    }

}