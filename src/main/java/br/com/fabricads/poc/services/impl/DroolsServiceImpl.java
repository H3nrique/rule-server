package br.com.fabricads.poc.services.impl;

import br.com.fabricads.poc.config.drools.DroolsUtil;
import br.com.fabricads.poc.services.DroolsService;
import lombok.extern.slf4j.Slf4j;
import org.drools.core.impl.KnowledgeBaseImpl;
import org.infinispan.client.hotrod.RemoteCache;
import org.infinispan.client.hotrod.RemoteCacheManager;
import org.kie.api.definition.type.FactType;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.EntryPoint;
import org.kie.api.runtime.rule.FactHandle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
@Slf4j
@Component
public class DroolsServiceImpl implements DroolsService {

    @Autowired
    private KieSession kieSession;

    // @Autowired
    // private RemoteCacheManager remoteCacheManager;

    // @PostConstruct
    // public void init() {
    //     // Insere objetos do JDG na KieSession
    //     try {
    //         RemoteCache<Object, Object> exampleCache = remoteCacheManager.getCache("exampleCache");
    //         for (Object item : exampleCache.values()) {
    //             kieSession.insert(item);
    //         }
    //     } catch (Exception err) {
    //         if(log.isDebugEnabled()) {
    //             log.error("Erro while loading KieSession with cache objects.", err);
    //         } else {
    //             log.error("Erro while loading KieSession with cache objects.");
    //         }
    //     }
    // }

    @Override
    public <T> T fireFules(T model) {
        log.debug("KieBase details :: {}", DroolsUtil.kbaseDetails(kieSession.getKieBase()));
        FactHandle factHandle = kieSession.insert(model);
        int rulesFired = kieSession.fireAllRules();
        kieSession.delete(factHandle);
        log.debug("Rules fired [{}]", rulesFired);
        return model;
    }

    @Override
    public <T> T fireFules(T model, String entryPoint) {
        log.debug("KieBase details :: {}", DroolsUtil.kbaseDetails(kieSession.getKieBase()));
        EntryPoint kieSessionEntryPoint = kieSession.getEntryPoint(entryPoint);
        kieSessionEntryPoint.insert(model);
        int rulesFired = kieSession.fireAllRules();
        log.debug("Rules fired [{}]", rulesFired);
        return model;
    }

    @Override
    public FactType getFactType(String packageName, String typeName) {
        return kieSession.getKieBase().getFactType(packageName, typeName);
    }
}