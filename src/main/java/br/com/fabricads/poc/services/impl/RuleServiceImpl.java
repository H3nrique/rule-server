package br.com.fabricads.poc.services.impl;

import br.com.fabricads.poc.services.DroolsService;
import br.com.fabricads.poc.services.RuleService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.apache.commons.beanutils.ConvertUtilsBean;
import org.kie.api.definition.type.FactType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Map;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
@Slf4j
@Component
public class RuleServiceImpl implements RuleService {

	@Autowired
	private DroolsService droolsService;

	@Override
	public String fireFules(String model) {
		log.debug("json :: {}", model);
		GsonJsonParser jsonParser = new GsonJsonParser();
		Map<String, Object> jsonObject = jsonParser.parseMap(model);
		log.debug("jsonObject :: {}", jsonObject);

		if(jsonObject.containsKey("packageName") && jsonObject.containsKey("typeName")) {
			return fireRulesComplexObject(jsonObject);
		}

		Map<String, Object> resultObjectMap = droolsService.fireFules(jsonObject);

		log.debug("resultObjectMap :: {}", resultObjectMap);
		GsonBuilder gsonBuilder = GsonBuilderUtils.gsonBuilderWithBase64EncodedByteArrays();
		Gson gson = gsonBuilder.setPrettyPrinting().create();
		return gson.toJson(resultObjectMap);
	}

	private String fireRulesComplexObject(Map<String, Object> jsonObject) {
		Object factInstance = populateJsonObject(jsonObject);
		Object resultObject = null;
		if(jsonObject.containsKey("entryPoint")) {
			String entryPoint = jsonObject.get("entryPoint").toString();
			log.debug("entryPoint:: {}", entryPoint);
			resultObject = droolsService.fireFules(factInstance, entryPoint);
		} else {
			resultObject = droolsService.fireFules(factInstance);
		}
		log.debug("resultObject :: {}", resultObject);
		GsonBuilder gsonBuilder = GsonBuilderUtils.gsonBuilderWithBase64EncodedByteArrays();
		Gson gson = gsonBuilder.setPrettyPrinting().create();
		return gson.toJson(resultObject);
	}

	private Object populateJsonObject(final Map<String, Object> jsonObject) {
		try {
			String packageName = jsonObject.get("packageName").toString();
			String typeName = jsonObject.get("typeName").toString();
			FactType factType = null;
			try {
				factType = droolsService.getFactType(packageName, typeName);
			} catch (UnsupportedOperationException err) {
				// left blank
			}
			final Object factInstance = factType != null ? factType.newInstance() : Class.forName(packageName.concat(".").concat(typeName)).newInstance();
			jsonObject.keySet().stream()
					.filter(key -> !key.equalsIgnoreCase("packageName") && !key.equalsIgnoreCase("typeName") && !key.equalsIgnoreCase("entryPoint"))
					.forEach(key -> {
						try {
							BeanUtilsBean beanUtilsBean = new BeanUtilsBean(new ConvertUtilsBean() {
								@Override
								public Object convert(String value, Class clazz) {
									if (clazz.isEnum()){
										return Enum.valueOf(clazz, value);
									}else{
										return super.convert(value, clazz);
									}
								}
							});
							beanUtilsBean.setProperty(factInstance, key, jsonObject.get(key).toString().contains("packageName") && jsonObject.get(key).toString().contains("typeName") ? populateJsonObject((Map<String, Object>) jsonObject.get(key)) : jsonObject.get(key));
						} catch (Exception err) {
							throw new RuntimeException(err);
						}
					});
			try {
				// All fields with timestamp will be set with the current date and time.
				BeanUtils.setProperty(factInstance, "timestamp", Calendar.getInstance().getTime());
			} catch (Exception err) {
				// left blank
			}
			return factInstance;
		} catch (Exception err) {
			throw new RuntimeException(err);
		}
	}
}