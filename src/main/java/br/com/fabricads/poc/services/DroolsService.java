package br.com.fabricads.poc.services;

import org.kie.api.definition.type.FactType;

/**
 * Created by Paulo Henrique Alves on 21/12/16.
 */
public interface DroolsService {
	<T> T fireFules(T model);
	<T> T fireFules(T model, String entryPoint);
	FactType getFactType(String packageName, String className);
}