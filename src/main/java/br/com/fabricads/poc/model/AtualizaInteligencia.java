package br.com.fabricads.poc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.infinispan.protostream.annotations.ProtoDoc;
import org.infinispan.protostream.annotations.ProtoField;

import java.io.Serializable;

/**
 * Created by h3nrique on 01/02/17.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ProtoDoc("@Indexed")
public class AtualizaInteligencia implements Serializable {

    @Setter
    private CodigoPostal codigoPostal;

    @ProtoField(number = 1, required = true)
    public CodigoPostal getCodigoPostal() {
        return codigoPostal;
    }
}
