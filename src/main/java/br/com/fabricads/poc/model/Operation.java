package br.com.fabricads.poc.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
public enum Operation implements Serializable {

    DEBIT, CREDIT;

}

