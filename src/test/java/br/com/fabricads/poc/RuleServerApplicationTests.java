package br.com.fabricads.poc;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Map;

@Slf4j
@WebAppConfiguration
@IntegrationTest("server.port:9080")
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = RuleServerApplication.class)
public class RuleServerApplicationTests {

	@Value("${server.port}")
	private int port;

	private String urlString;
	private TestRestTemplate restTemplate;
    private HttpHeaders headers;

	@Before
	public void before() {
		restTemplate = new TestRestTemplate();
		urlString = "http://localhost:" + port + "/rule/process/generic";
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
	}

	@Test
	public void simplePostalCodeTest() {
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("json", "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"ItemPostal\",\"cep\":\"60510101\"}");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<String> responseEntity = restTemplate.postForEntity(urlString, request, String.class);
		Assert.assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
		Assert.assertNotNull(responseEntity.getBody());
		log.info("Rule results :: {} ", responseEntity.getBody());
	}

	@Test
	public void spreadsheetTest() {
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("json", "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"ObjetoEntrada\",\"ncm\":\"300\",\"descricao\":\"CAMISA FARDA UNIFORME\",\"cnae\":\"100\"}");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<String> responseEntity = restTemplate.postForEntity(urlString, request, String.class);
		Assert.assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
		Assert.assertNotNull(responseEntity.getBody());
		log.info("Rule results :: {} ", responseEntity.getBody());
	}

	@Test
	public void complexEventTest() {
		MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
		map.add("json", "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"Transaction\",\"id\":\"1\",\"value\":\"10\",\"operation\":\"DEBIT\",\"entryPoint\":\"BankStream\"}");

		HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		ResponseEntity<String> responseEntity = restTemplate.postForEntity(urlString, request, String.class);
		Assert.assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
		Assert.assertNotNull(responseEntity.getBody());
		log.info("Rule results 1 :: {} ", responseEntity.getBody());

		GsonJsonParser jsonParser = new GsonJsonParser();
		Map<String, Object> jsonObject = jsonParser.parseMap(responseEntity.getBody());
		Assert.assertTrue(jsonObject.get("denied") == Boolean.FALSE);

		try {
			log.info("Waiting 3sec to execute other request.");
			// wait 3s
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		map= new LinkedMultiValueMap<String, String>();
		map.add("json", "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"Transaction\",\"id\":\"1\",\"value\":\"10\",\"operation\":\"DEBIT\",\"entryPoint\":\"BankStream\"}");

		request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		responseEntity = restTemplate.postForEntity(urlString, request, String.class);
		Assert.assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
		Assert.assertNotNull(responseEntity.getBody());
		log.info("Rule results 2 :: {} ", responseEntity.getBody());

		jsonObject = jsonParser.parseMap(responseEntity.getBody());
		Assert.assertTrue(jsonObject.get("denied") == Boolean.TRUE);

		try {
			log.info("Waiting 3sec to execute other request.");
			// wait 3s
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		map= new LinkedMultiValueMap<String, String>();
		map.add("json", "{\"packageName\":\"br.com.fabricads.poc.model\",\"typeName\":\"Transaction\",\"id\":\"2\",\"value\":\"10\",\"operation\":\"DEBIT\",\"entryPoint\":\"BankStream\"}");

		request = new HttpEntity<MultiValueMap<String, String>>(map, headers);
		responseEntity = restTemplate.postForEntity(urlString, request, String.class);
		Assert.assertEquals(HttpStatus.ACCEPTED, responseEntity.getStatusCode());
		Assert.assertNotNull(responseEntity.getBody());
		log.info("Rule results 3 :: {} ", responseEntity.getBody());

		jsonObject = jsonParser.parseMap(responseEntity.getBody());
		Assert.assertTrue(jsonObject.get("denied") == Boolean.TRUE);
	}
}